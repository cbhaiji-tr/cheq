![](https://res-2.cloudinary.com/crunchbase-production/image/upload/c_lpad,h_170,w_170,f_auto,b_white,q_auto:eco/v3fu3x2siv416h1nqjec)

## Overview

This is a tab bar navigation-based app that has 3 screens. Only the second one, wallet dashboard, is default and 'functional' one among the 3 screens. This screen has the following:

- A drop down to select a month for the details shown on the wallet dashboard
- A carousel for bank detail cards
  - A bar chart to represent the amounts spent for the last 4 months
  - A highlighted month for which the expense detail cards are shown below
- Expense card represents the expense category and the spent amount for the current bank and the selected month

## Task Requirements

- Data: make an API call using fake data as responses e.g. banking card details, expense details
- For the “banking cards” section, it should be a carousel slider that can slide left and right. This needs to be smooth with no lag.
- You can use any color or images. The charts need to be implemented with response data from the API call. At least one pie chart and one vertical bar chart should be implemented. You can use fake data.
- “This month” is a dropdown list that includes texts like Jan, Feb ... Dec
- Show blank page for the first tab page and Profile page. You can use the image as tab icons

## Structure

```
├── cheq
│ ├── Assets.xcassets
│ │ ├── AppIcon.appiconset
│ │ ├── comingSoon.imageset
│ │ ├── expense_categories
│ │ │ ├── food.imageset
│ │ │ ├── groceries.imageset
│ │ │ ├── household.imageset
│ │ │ └── transport.imageset
│ │ └── launchImage.imageset
│ ├── Components
│ │ ├── CollectionViewCells
│ │ │ ├── BankCard
│ │ │ └── ExpenseCard
│ │ ├── Customised
│ │ └── TableViewCells
│ ├── Models
│ │ └── Enums
│ │ ├── Expense
│ │ └── General
│ ├── Resources
│ ├── Screens
│ │ └── WalletDashboard
│ ├── Services
│ └── Utils
│ └── Extensions
└── cheqTests
├── Mocks
└── Tests
```

#### Structure details

The app is structured as per the `MVP` design pattern to make sure that the business logic resides in a class (`presenter` in our case) detached from view code and hence is unit testable.
We can also follow `MVVM`, `Clean Swift`, or other patterns depending on the project's requirement.

###### Components

A directory to hold all the `reusable` or `customized` components. Currently, collection and table view custom cells are added here. Also, the common customizations for the BarChartView have been added here.

###### Models

This directory is responsible for keeping all the `Models` and related `Enums` for the app

###### Resources

This directory will hold all the required resources like JSON files, SVGs, fonts, HTML files, etc. For now, this just hold the mocked responses for the bank details and the expense details

###### Screens

This directory is to keep directories for each screen of the app. Each directory has 3 files in general, a `view` file must be and XIB or Storyboard, a controller file, and a `presenter` file.

###### Services

This directory is to keep directories for each service of the app. Currently, this is blank as there is no such service added for this test task.

###### Utils

This directory is to keep all the code that will be used as a utility for the app. It also includes all the extensions for any Foundation or UIKit component. For now, it has a `ChartUtils` which has methods or values common for our app.

###### cheqTests

This is the directory of the app's Unit Tests target. It has 2 subdirectories, `Tests` and `Mocks`.
`Tests` will contain all the tests of the app and `Mocks` will contain the mocks we will create for our tests.

## Notes

- The current project has storyboards for creating the views, but we can also use SwiftUI which is newer
- The new combine framework can also be used for cleaner code
- CollectionView + TableView are used as the main components for the Wallet Dashboard to make it scalable
- Data for Bank Details and related Expense Details is hardcoded, hence the dashboard will always have 2 bank detail cards and 4 expense cards.
- Expense cards for the 2nd bank card will not be 'correct' as per the bank details
- Test cases are only added for Month enum and the `WalletDashboardPresenter`

## Build Info

- iOS 13.0+
- Xcode 11.6
- Swift 5

## Project Setup

> CocoaPods is our dependency manager. If it is not installed on the machine please visit: https://guides.cocoapods.org/using/getting-started.html

- Go to the project's root directory on the mac's terminal
- Run `pod install`
- Make sure to open `cheq.xcworkspace` and then run the app

## Project Dependencies

- [Charts](https://github.com/danielgindi/Charts)
- [Dropdown](https://github.com/AssistoLab/DropDown)

# Demo

![](https://gitlab.com/cbhaiji-tr/cheq/-/raw/master/cheq.gif)

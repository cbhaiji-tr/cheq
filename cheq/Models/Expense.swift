import Foundation

struct ExpenseAmount: Codable {
    let value: Decimal
    let currency: Currency

    func displayValue() ->  String {
        return currency.symbol + value.description
    }
}

struct Expense: Codable {
    let category: ExpenseCategory
    let amount: ExpenseAmount
}

struct MonthlyExpense: Codable {
    let month: Month
    let amount: ExpenseAmount
}

struct BankDetail: Codable {
    let id: String
    let name: String
    let monthlyExpenses: [MonthlyExpense]
}

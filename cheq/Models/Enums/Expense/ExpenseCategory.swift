import Foundation
import UIKit.UIImage

enum ExpenseCategory: String, Codable {
    case household
    case groceries
    case food
    case transport

    var title: String {
        self.rawValue.capitalized
    }

    var icon: UIImage {
        switch self {
        case .household:
            return #imageLiteral(resourceName: "household")
        case .groceries:
            return #imageLiteral(resourceName: "groceries")
        case .food:
            return #imageLiteral(resourceName: "food")
        case .transport:
            return #imageLiteral(resourceName: "transport")
        }
    }

    var color: UIColor {
        switch self {
        case .household:
            return UIColor.systemIndigo
        case .groceries:
            return UIColor.systemTeal
        case .food:
            return UIColor.systemYellow
        case .transport:
            return UIColor.systemGreen
        }
    }
}

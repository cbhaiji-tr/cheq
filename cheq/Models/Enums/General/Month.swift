
import Foundation

enum Month: Int, Codable, CaseIterable {
    case January = 1, February, March, April, May, June,
    July, August, September, October, November, December

    func displayName(locale: NSLocale = NSLocale.autoupdatingCurrent as NSLocale) -> String {
        let formatter = DateFormatter()
        formatter.locale = locale as Locale
        formatter.dateFormat = "MM"
        let date = formatter.date(from: "\(self.rawValue)")
        formatter.dateFormat = "MMM"
        return formatter.string(from: date!)
    }

    static var currentMonth: Month {
        return Month(rawValue: Date().monthNumber) ?? .January
    }

    static var monthsTillDate: [Month] {
        return Month.allCases.filter({ $0.rawValue <= Date().monthNumber })
    }

    static func getLastMonths(n: Int) -> [Month] {
        let allMonthsTillDate = Month.allCases.filter({ $0.rawValue <= Date().monthNumber })
        return allMonthsTillDate.suffix(n)
    }
}

// MARK: - Printable protocol

extension Month: CustomStringConvertible {
    public var description: String {
        return self.displayName()
    }
}

// MARK: - DebugPrintable protocol

extension Month: CustomDebugStringConvertible {
    public var debugDescription: String {
        return self.description
    }
}

// MARK: - Equatable protocol

extension Month: Equatable {}

func ==(lhs: Month, rhs: Month) -> Bool {
    return lhs.rawValue == rhs.rawValue
}

import Foundation

enum ExpenseCurrency: String, Codable {
    case usd
    case inr
    case gbp

    var symbol: String {
        switch self {
        case .usd:
            return "$"
        case .inr:
            return "₹"
        case .gbp:
            return "£"
        }
    }
}

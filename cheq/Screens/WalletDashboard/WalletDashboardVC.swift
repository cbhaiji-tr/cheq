import UIKit
import DropDown

class WalletDashboardVC: UIViewController {

    // MARK: - Outlets

    @IBOutlet weak var selectedMonthButton: UIButton!
    @IBOutlet weak var dashboardTableView: UITableView!

    // MARK: - Variables and Constants

    private var presenter: WalletDashboardPresenter!

    lazy var monthsDropDown: DropDown = {
        let dropDown = DropDown(anchorView: self.selectedMonthButton)
        // Set data source for the drop down
        dropDown.dataSource = self.presenter?.monthNames ?? []
        // Setup delegate for the value selection
        dropDown.selectionAction = { (index, _) in
            self.presenter?.selectMonth(index: index)
        }
        return dropDown
    }()

    // MARK: View's Lifecycle Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        // Initialise presenter
        self.presenter = WalletDashboardPresenter(service: self)
        // Setup collection views
        self.setupTableView()
        // Setup button for selecting month
        self.setupButtonForMonthSelection()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Hide navigation bar
        self.navigationController?.isNavigationBarHidden = true
    }

    // MARK: Action Methods

    @IBAction func openMonthsDropDown() {
        // Set selected month for the drop down
        if let indexToBeSelected = monthsDropDown.dataSource
            .firstIndex(of: self.presenter.selectedMonth.displayName()) {
            monthsDropDown.selectRow(indexToBeSelected)
        }
        // Show the drop down
        self.monthsDropDown.show()
    }

    // MARK: Helper Methods

    fileprivate func setupTableView() {
        // Regsiter cells
        self.dashboardTableView.registerCell(
            DashboardTableViewCell.self, reuseIdentifier: DashboardTableViewCell.reuseIndentifier
        )
        // Set delegate and datasource
        self.dashboardTableView.delegate = self
        self.dashboardTableView.dataSource = self
    }

    fileprivate func setupButtonForMonthSelection() {
        // Setup button
        self.selectedMonthButton.setImageToRight()
    }
}

extension WalletDashboardVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return WalletSection.allCases.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let walletSection = WalletSection(rawValue: indexPath.section),
            let data = presenter?.getData(for: walletSection),
            let cell = tableView.dequeueReusableCell(withIdentifier: DashboardTableViewCell.reuseIndentifier)
            as? DashboardTableViewCell else { return UITableViewCell() }

        cell.sectionType = walletSection
        cell.data = data
        cell.selectedMonth = self.presenter.selectedMonth

        return cell
    }
}

extension WalletDashboardVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let walletSection = WalletSection(rawValue: indexPath.section)
            else { return 0 }
        return self.presenter?.getRowHeight(for: walletSection) ?? 0
    }
}

extension WalletDashboardVC: WalletDashboardPresenterService {

    func updateSelectedMonth(month: Month) {
        selectedMonthButton.setTitle(month.displayName(), for: .normal)
        let indexSet = IndexSet(arrayLiteral: WalletSection.bankCardWithBarChart.rawValue)
        self.dashboardTableView.reloadSections(indexSet, with: .none)
    }

    func refreshSection(section: WalletSection) {
        let indexSet = IndexSet(arrayLiteral: section.rawValue)
        self.dashboardTableView.reloadSections(indexSet, with: .none)
    }
}

import Foundation
import CoreGraphics
import UIKit

// MARK: - Enums

enum WalletSection: Int, CaseIterable {
    case bankCardWithBarChart
    case expenseCardWithPieChart
    case expenseCardWithCircularProgressBar

    var reuseIdentifier: String {
        switch self {
        case .bankCardWithBarChart:
            return BankCardCollectionViewCell.reuseIdentifier
        case .expenseCardWithCircularProgressBar:
            return ExpenseCardCollectionViewCell.reuseIdentifier
        case .expenseCardWithPieChart:
            return ExpenseCardCollectionViewCell.reuseIdentifier
        }
    }

    var cardHeight: CGFloat {
        switch self {
        case .bankCardWithBarChart:
            return 240
        case .expenseCardWithCircularProgressBar:
            return 200
        case .expenseCardWithPieChart:
            return 0
        }
    }

    var itemsPerRow: Int {
        switch self {
        case .bankCardWithBarChart:
            return 1
        case .expenseCardWithCircularProgressBar:
            return 2
        case .expenseCardWithPieChart:
            return 1
        }
    }

    var sectionInsets: UIEdgeInsets {
        switch self {
        case .bankCardWithBarChart:
            return UIEdgeInsets(top: 0, left: Constants.defaultPadding, bottom: 0, right: Constants.defaultPadding)
        case .expenseCardWithCircularProgressBar, .expenseCardWithPieChart:
            return UIEdgeInsets.all(with: Constants.defaultPadding)
        }
    }
}

// MARK: - Service

protocol WalletDashboardPresenterService: class {
    func refreshSection(section: WalletSection)
    func updateSelectedMonth(month: Month)
}

class WalletDashboardPresenter {

    // MARK: Variables and Constants

    private weak var service: WalletDashboardPresenterService?

    private var months: [Month] {
        return Month.getLastMonths(n: Constants.numberOfMonthsForDashboard)
    }

    var monthNames: [String] {
        return months.compactMap({ $0.displayName() })
    }

    private(set) var bankDetails: [BankDetail] = [] {
        didSet {
            self.service?.refreshSection(section: .bankCardWithBarChart)
        }
    }

    private(set) var expenseDetails: [Expense] = [] {
        didSet {
            self.service?.refreshSection(section: .expenseCardWithCircularProgressBar)
        }
    }

    private(set) var selectedMonth: Month {
        didSet {
            self.service?.updateSelectedMonth(month: self.selectedMonth)
            self.getExpenseDetails()
        }
    }

    // MARK: Init Methods

    init(service: WalletDashboardPresenterService) {
        self.service = service
        self.selectedMonth = Month.currentMonth
        getBankDetails()
        getExpenseDetails()
    }

    // MARK: - Methods for Controller

    func getData(for section: WalletSection) -> Any {
        switch section {
        case .bankCardWithBarChart:
            return self.bankDetails
        case .expenseCardWithCircularProgressBar:
            return self.expenseDetails
        case .expenseCardWithPieChart:
            return []
        }
    }

    func getRowHeight(for section: WalletSection) -> CGFloat {
        switch section {

        case .bankCardWithBarChart, .expenseCardWithPieChart:
            return section.cardHeight

        case .expenseCardWithCircularProgressBar:
            // Cacluate number of rows for the expense card collection
            return (self.expenseDetails.count > section.itemsPerRow)
            ? CGFloat(self.expenseDetails.count / section.itemsPerRow) * section.cardHeight
            : section.cardHeight
        }
    }

    // MARK: - Private Methods

    func getBankDetails() {
        self.bankDetails = [BankDetail].parse(jsonFile: "BankDetails") ?? []
    }

    func getExpenseDetails() {
        self.expenseDetails = [Expense].parse(jsonFile: "Expenses_\(selectedMonth.displayName().lowercased())") ?? []
    }

    func selectMonth(index: Int) {
        self.selectedMonth = self.months[index]
    }
}

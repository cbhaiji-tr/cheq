import Foundation
import UIKit.UIButton
import UIKit.UIImage

extension UIButton {
    func setImageToRight(padding: CGFloat = 20) {
        guard let imageViewWidth = self.imageView?.frame.size.width else { return }
        self.titleEdgeInsets = UIEdgeInsets(
            top: 0, left: padding - imageViewWidth, bottom: 0, right: imageViewWidth + padding + 8
        )
        self.imageEdgeInsets = UIEdgeInsets(
            top: 0, left: (self.frame.width - (imageViewWidth + padding)), bottom: 0, right: 0
        )
    }
}


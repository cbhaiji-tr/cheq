import UIKit

extension UIEdgeInsets {
    static func all(with value: CGFloat) -> UIEdgeInsets {
        UIEdgeInsets(top: value, left: value, bottom: value, right: value)
    }
}

import UIKit

extension UITableView {
    func registerCell<T: UITableViewCell>(_ cell: T.Type, reuseIdentifier: String) {
        let cellNib = UINib(nibName: String(describing: cell.self), bundle: nil)
        self.register(cellNib, forCellReuseIdentifier: reuseIdentifier)
    }
}

import Foundation
import UIKit

struct Constants {
    static let cornerRadius: CGFloat = 20
    static let defaultPadding: CGFloat = 20
    static let numberOfMonthsForDashboard = 4
}

struct ChartConstants {

    // MARK: - Colors

    static let labelTextColor: UIColor = UIColor.white
    static let highlightColor: UIColor = UIColor.white
    static let colors: [UIColor] = [UIColor.white.withAlphaComponent(0.3)]
    static let valueColors: [UIColor] = [UIColor.white]

    // MARK: - Fonts

    static let valueFont: UIFont = .systemFont(ofSize: 12)
    static let labelFont: UIFont = .systemFont(ofSize: 12)
}

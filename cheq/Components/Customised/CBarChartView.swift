import Foundation
import Charts

class CBarChartView: BarChartView {

    override func awakeFromNib() {
        super.awakeFromNib()

        let leftAxis = self.leftAxis
        leftAxis.drawAxisLineEnabled = false
        leftAxis.drawGridLinesEnabled = false
        leftAxis.drawLabelsEnabled = false

        let rightAxis = self.rightAxis
        rightAxis.drawAxisLineEnabled = false
        rightAxis.drawGridLinesEnabled = false
        rightAxis.drawLabelsEnabled = false

        let xAxis = self.xAxis
        xAxis.drawAxisLineEnabled = false
        xAxis.drawGridLinesEnabled = false
        xAxis.labelPosition = .bottom
        xAxis.labelFont = ChartConstants.labelFont
        xAxis.labelTextColor = ChartConstants.labelTextColor

        let yAxis = self.xAxis
        yAxis.drawAxisLineEnabled = false
        yAxis.drawGridLinesEnabled = false
        yAxis.drawLabelsEnabled = false

        self.legend.enabled = false
        self.pinchZoomEnabled = false
        self.doubleTapToZoomEnabled = false
        self.highlightPerTapEnabled = false
        self.highlightPerDragEnabled = false
    }
}

import Foundation
import Charts

class CBarChartDataSet: BarChartDataSet {
    required init() { super.init() }

    override init(entries: [ChartDataEntry]?, label: String?) {
        super.init(entries: entries, label: label)
        // Setup colors
        self.valueColors = ChartConstants.valueColors
        self.colors = ChartConstants.colors
        self.highlightColor = ChartConstants.highlightColor
        // Setup font
        self.valueFont = ChartConstants.valueFont
        // Setup value formatter
        self.valueFormatter = DefaultValueFormatter(formatter: CChartUtils.getFormatterForCurrency())
    }
}

import UIKit
import HGCircularSlider

class ExpenseCardCollectionViewCell: UICollectionViewCell {

    static let reuseIdentifier: String = "expenseCardCollectionViewCell"

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var progressContainerView: CircularSlider!
    @IBOutlet weak var amountValueLabel: UILabel!
    @IBOutlet weak var percentageLabel: UILabel!

    var totalExpenseAmount: Double? {
        didSet {
            if expenseDetail != nil {
                // Set progress view
                self.progressContainerView.trackFillColor = expenseDetail?.category.color ?? UIColor.systemIndigo
                self.progressContainerView.maximumValue = CGFloat(totalExpenseAmount ?? 0)
                self.progressContainerView.endPointValue = CGFloat(expenseDetail?.amount.value.doubleValue ?? 0)
                // Set labels
                self.amountValueLabel.text = expenseDetail?.amount.displayValue()
                self.percentageLabel.text = getPercentage(
                    of: self.progressContainerView.endPointValue, with: self.progressContainerView.maximumValue
                )
            }
        }
    }

    var expenseDetail: Expense? {
        didSet {
            self.titleLabel.text = expenseDetail?.category.title
            self.iconImageView.image = expenseDetail?.category.icon.withRenderingMode(.alwaysTemplate)
            self.iconImageView.tintColor = expenseDetail?.category.color ?? UIColor.systemIndigo
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Set corner radius for the card
        self.contentView.layer.cornerRadius = Constants.cornerRadius
        // Customise UI for the icon
        guard let iconWraperView = self.iconImageView.superview else { return }
        iconWraperView.layer.cornerRadius = iconWraperView.frame.height/2
    }

    // MARK: Helper Methods

    private func getPercentage(of value: CGFloat, with totalValue: CGFloat) -> String {
        let percentageValue = (value / totalValue) * 100
        return Int(percentageValue).description + "%"
    }
}

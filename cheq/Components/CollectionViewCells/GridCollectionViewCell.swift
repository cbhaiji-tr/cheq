import UIKit

class ExpenseCardCollectionViewCell: UICollectionViewCell {

    static let reuseIdentifier: String = "expenseCardCollectionViewCell"

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var chartContainerView: UIView!
}

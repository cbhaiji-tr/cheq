import UIKit
import Charts

class BankCardCollectionViewCell: UICollectionViewCell {

    // MARK: - Class Constants

    static let reuseIdentifier: String = "bankCardCollectionViewCell"

    // MARK: - Outlets

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var barChartView: CBarChartView!

    // MARK: - Variables

    var bankDetail: BankDetail! {
        didSet {
            self.titleLabel.text = self.bankDetail.name
            self.setupChart()
        }
    }

    var barChartData: BarChartData {
        let months = bankDetail.monthlyExpenses.compactMap({ $0.month.displayName() })
        let amounts = bankDetail.monthlyExpenses.compactMap({ $0.amount.value.doubleValue })
        // NOTE: Faced some issues while moving this to the Custom class for BarChartView
        let xAxis = self.barChartView.xAxis
        xAxis.drawLabelsEnabled = true
        xAxis.labelCount = months.count
        xAxis.valueFormatter = IndexAxisValueFormatter(values: months)

        let dataEntries = amounts.enumerated().map({ BarChartDataEntry(x: Double($0.offset), y: $0.element) })
        let dataSet = CBarChartDataSet(entries: dataEntries)

        return BarChartData(dataSet: dataSet)
    }

    // MARK: - Lifecycle Methods

    override func awakeFromNib() {
        super.awakeFromNib()

        self.contentView.layer.cornerRadius = Constants.cornerRadius
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        // Add gradient
        self.contentView.addGradient(colors: [UIColor.systemIndigo, UIColor.systemPurple, UIColor.systemIndigo])
    }

    // MARK: - Private Methods

    private func setupChart() {
        self.barChartView.data = self.barChartData
    }

    // MARK: - Helper Methods

    func setHighlighted(month: Month = Month.currentMonth) {
        guard let indexForSelectedMonth = self.bankDetail.monthlyExpenses
            .firstIndex(where: { $0.month == month }) else { return }
        let highlight = Highlight(
            x: Double(indexForSelectedMonth),
            y: self.bankDetail.monthlyExpenses[indexForSelectedMonth].amount.value.doubleValue,
            dataSetIndex: 0
        )
        self.barChartView.highlightValue(highlight)
    }
}

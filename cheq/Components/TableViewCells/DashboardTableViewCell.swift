import UIKit

class DashboardTableViewCell: UITableViewCell {

    // MARK: - Class Properties

    static let reuseIndentifier = "DashboardTableViewCell"

    // MARK: - Outlets

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!

    // MARK: - Variables and Constants

    var sectionType: WalletSection? {
        didSet {
            if data != nil {
                self.collectionView.reloadData()
                self.updatePageControl()
            }
        }
    }

    var data: Any? {
        didSet {
            if sectionType != nil {
                self.collectionView.reloadData()
                self.updatePageControl()
            }
        }
    }

    var selectedMonth: Month = Month.currentMonth {
        didSet {
            if data != nil && sectionType != nil {
                self.collectionView.reloadData()
            }
        }
    }

    // MARK: - Lifecycle Methods

    override func awakeFromNib() {
        super.awakeFromNib()
        // Setup collection view
        self.setupCollectionView()
        // Disbale the page controller action/ Will be updated on slide of carousel
        self.pageControl.isUserInteractionEnabled = false
    }

    // MARK: - Private Helper Methods

    private func setupCollectionView() {
        // Set delegate and data source
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        // Regsiter cells
        self.collectionView.registerCell(
            BankCardCollectionViewCell.self, reuseIdentifier: BankCardCollectionViewCell.reuseIdentifier
        )
        self.collectionView.registerCell(
            ExpenseCardCollectionViewCell.self, reuseIdentifier: ExpenseCardCollectionViewCell.reuseIdentifier
        )
    }

    private func updatePageControl() {
        if self.sectionType == WalletSection.bankCardWithBarChart {
            self.pageControl.numberOfPages = (self.data as? Array<Any>)?.count ?? 0
        } else {
            // No need to show page control for section other than the bank cards
            self.pageControl.numberOfPages = 0
        }
    }

    // MARK: - SrollView Methods

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offSet = scrollView.contentOffset.x
        let width = scrollView.frame.width
        let horizontalCenter = width / 2
        // Set page control only when the cell is scrolled more than half
        pageControl.currentPage = Int(offSet + horizontalCenter) / Int(width)
    }
}

extension DashboardTableViewCell: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.data as? Array<Any>)?.count ?? 0
    }

    func collectionView(
        _ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath
    ) -> UICollectionViewCell {
        guard let _section = self.sectionType, let _data = data,
            let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
            else { return UICollectionViewCell() }

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: _section.reuseIdentifier, for: indexPath)

        switch _section {
        case .bankCardWithBarChart:
            guard let bankDetails = _data as? [BankDetail],
                let bankCardCell = cell as? BankCardCollectionViewCell else { return UICollectionViewCell() }
            bankCardCell.bankDetail = bankDetails[indexPath.row]
            bankCardCell.setHighlighted(month: self.selectedMonth)
            // Update layout's scroll direction
            layout.scrollDirection = .horizontal

        case .expenseCardWithCircularProgressBar:
            guard let expenseDetails = _data as? [Expense],
                let expenseCardCell = cell as? ExpenseCardCollectionViewCell else { return UICollectionViewCell() }
            expenseCardCell.expenseDetail = expenseDetails[indexPath.row]
            expenseCardCell.totalExpenseAmount = expenseDetails
                .compactMap({ $0.amount.value.doubleValue }).reduce(0, +)
            // Update layout's scroll direction
            layout.scrollDirection = .vertical

        case .expenseCardWithPieChart:
            break
        }

        return cell
    }
}

extension DashboardTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {

        guard let _section = self.sectionType else { return CGSize.zero }

        let horizontalPadding = _section.sectionInsets.left * CGFloat(_section.itemsPerRow + 1)
        let verticalPadding = _section.sectionInsets.top * CGFloat(_section.itemsPerRow + 1)

        switch _section {
        case .bankCardWithBarChart:
            return CGSize(
                width: self.collectionView.frame.width - horizontalPadding,
                height: self.collectionView.frame.height - verticalPadding
            )

        case .expenseCardWithCircularProgressBar:
            let width = (self.collectionView.frame.width - horizontalPadding)/2
            let height = (self.collectionView.frame.height - verticalPadding)/2
            return CGSize(width: width, height: height)

        case .expenseCardWithPieChart:
            return CGSize.zero
        }
    }

    func collectionView(
        _ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
        insetForSectionAt section: Int
    ) -> UIEdgeInsets {
        return self.sectionType?.sectionInsets ?? UIEdgeInsets.zero
    }

    func collectionView(
        _ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
        minimumLineSpacingForSectionAt section: Int
    ) -> CGFloat {
        return self.sectionType?.sectionInsets.left ?? Constants.defaultPadding
    }
}

import Foundation
@testable import cheq

class MockWalletDashboardService: WalletDashboardPresenterService {
    func refreshSection(section: WalletSection) {}
    func updateSelectedMonth(month: Month) {}
}

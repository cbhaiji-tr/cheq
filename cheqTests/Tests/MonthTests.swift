import XCTest
@testable import cheq

class MonthTests: XCTestCase {

    var monthNumbers: [Int] = []
    var monthNames: [String] = []

    // MARK: Overriden Methods

    override func setUp() {
        super.setUp()

        monthNumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    }

    override func tearDown() {
        super.tearDown()

        self.monthNumbers = []
        self.monthNames = []
    }

    // MARK: Tests

    func testMonthNumbers() {
        for (index, month) in Month.allCases.enumerated() {
            XCTAssert(monthNumbers[index] == month.rawValue, "Incorrect month number for \(month.displayName())")
        }
    }

    func testMonthNames() {
        for (index, month) in Month.allCases.enumerated() {
            XCTAssert(monthNames[index] == month.displayName(), "Incorrect month name for \(month.displayName())")
        }
    }

    func testCurrentMonth() {
        let currentMonth = Month.currentMonth

        XCTAssert(Date().monthNumber == currentMonth.rawValue, "Incorrect current month")
    }

    func testMonthsTillDate() {
        let monthNumbersTillDate = monthNumbers.filter({ $0 <= Date().monthNumber })
        let monthsTillDate = Month.monthsTillDate

        for (index, month) in monthsTillDate.enumerated() {
            XCTAssert(monthNumbersTillDate[index] == month.rawValue, "Incorrect months till date")
        }
    }

    func testGetLastMonths() {
        let numberOfMonthsNeeded = 3
        let monthNumbersTillDate: [Int] = monthNumbers.filter({ $0 <= Date().monthNumber }).suffix(numberOfMonthsNeeded)

        for (index, month) in Month.getLastMonths(n: numberOfMonthsNeeded).enumerated() {
            XCTAssert(monthNumbersTillDate[index] == month.rawValue, "Incorrect months till date")
        }
    }
}

import XCTest
@testable import cheq

class WalletDashboardPresenterTests: XCTestCase {

    let mockedService = MockWalletDashboardService()

    var presenter: WalletDashboardPresenter?

    // MARK: Overriden Methods

    override func setUp() {
        super.setUp()

        self.presenter = WalletDashboardPresenter(service: mockedService)
    }

    override func tearDown() {
        super.tearDown()

        self.presenter = nil
    }

    // MARK: - Tests

    func testPresenterInitialisation() {
        XCTAssertNotNil(presenter)
    }

    func testPresenterProperties() {
        XCTAssert(self.presenter!.selectedMonth == Month(rawValue: Date().monthNumber)!)
        // Hardcoded for now as we have the mocked data for both bankDetails and expenseDetails
        XCTAssert(self.presenter!.bankDetails.count == 2)
        XCTAssert(self.presenter!.expenseDetails.count == 4)
    }

    func testGetRowHeight() {
        XCTAssert(self.presenter!.getRowHeight(for: .bankCardWithBarChart) == 240)
    }

    func testGetDataForSection() {
        XCTAssert(self.presenter!.getData(for: .bankCardWithBarChart) is [BankDetail])
        XCTAssert(self.presenter!.getData(for: .expenseCardWithCircularProgressBar) is [Expense])
    }

    func testSelectMonth() {
        let randomMonth = self.presenter!.monthNames.randomElement()!
        let indexOfRandomMonth = self.presenter!.monthNames.firstIndex(of: randomMonth)!

        self.presenter?.selectMonth(index: indexOfRandomMonth)

        XCTAssert(self.presenter!.selectedMonth.displayName() == randomMonth)
    }
}
